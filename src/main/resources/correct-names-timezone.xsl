<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="no" indent="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:template match="node()">
		<xsl:copy>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="Shipment">
		<shipment>
			<xsl:apply-templates select="node()"/>
		</shipment>
	</xsl:template>

	<xsl:template match="Item">
		<item>
			<xsl:apply-templates select="node()"/>
		</item>
	</xsl:template>
	
	<xsl:template match="TrackingEvent">
		<event>
			<xsl:apply-templates select="node()"/>
		</event>
	</xsl:template>
	
	<xsl:template
		match="eventTime/text() | deliveryDate/text() | estimatedTimeOfArrival/text() 
		| deliveryDate/text() | dropOffDate/text() | originEstimatedTimeOfArrival/text()">
		<xsl:variable name="dateTime" select="."/>
		<xsl:variable name="dateTimeZone" select="concat($dateTime, '+01:00')"/>
		<xsl:value-of select="$dateTimeZone"/>
	</xsl:template>
	
</xsl:stylesheet>