package com.cgi.osd.restref.businesslogic;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.joda.time.DateTime;

@XmlRootElement(name = "NttResponse")
@XmlAccessorOrder(XmlAccessOrder.UNDEFINED)
@XmlAccessorType(XmlAccessType.FIELD)
public class NttResponse {

    @XmlElement()
    private String xtreamString = "The extrem string";

    @XmlElement()
    private String header = "This is a header";

    @XmlElementWrapper(name = "items")
    @XmlElement(name = "Item")
    private List<NttResponseItem> items = new ArrayList<>();

    private String nullString = null;

    private DateTime nullDateTime = null;

    private transient int qualifier = 1;

    public String getHeader() {
	return this.header;
    }

    public List<NttResponseItem> getItems() {
	return this.items;
    }

    public DateTime getNullDateTime() {
	return this.nullDateTime;
    }

    public String getNullString() {
	return this.nullString;
    }

    public int getQualifier() {
	return this.qualifier;
    }

    public String getXtreamString() {
	return this.xtreamString;
    }

    public void setHeader(String header) {
	this.header = header;
    }

    public void setItems(List<NttResponseItem> items) {
	this.items = items;
    }

    public void setNullDateTime(DateTime nullDateTime) {
	this.nullDateTime = nullDateTime;
    }

    public void setNullString(String nullString) {
	this.nullString = nullString;
    }

    public void setQualifier(int qualifier) {
	this.qualifier = qualifier;
    }

    public void setXtreamString(String xtreamString) {
	this.xtreamString = xtreamString;
    }

}
