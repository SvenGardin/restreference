package com.cgi.osd.restref.businesslogic;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;

import com.cgi.osd.restref.rest.resteasy.JodaJsonSerializer;
import com.cgi.osd.restref.rest.resteasy.NttDateTimeXmlAdapter;

@XmlRootElement(name = "NttResponseItem")
@XmlAccessorOrder(XmlAccessOrder.UNDEFINED)
@XmlAccessorType(XmlAccessType.FIELD)
public class NttResponseItem {

    @XmlElement
    private long itemId;

    @XmlElement
    private String itemName;

    private transient int qualifier;

    private BigDecimal weight = new BigDecimal(3.5);

    @XmlElement
    @XmlJavaTypeAdapter(NttDateTimeXmlAdapter.class)
    @JsonSerialize(using = JodaJsonSerializer.class)
    private DateTime deliveryTime;

    @XmlElement
    private String nullString = null;

    public NttResponseItem() {

    }

    public NttResponseItem(long itemId, String itemName, DateTime deliveryTime, BigDecimal weight) {
	super();
	this.itemId = itemId;
	this.itemName = itemName;
	this.deliveryTime = deliveryTime;
	this.weight = weight;
    }

    public DateTime getDeliveryTime() {
	return this.deliveryTime;
    }

    public long getItemId() {
	return this.itemId;
    }

    public String getItemName() {
	return this.itemName;
    }

    public String getNullString() {
	return this.nullString;
    }

    public BigDecimal getWeight() {
	return this.weight;
    }

    public void setDeliveryTime(DateTime deliveryTime) {
	this.deliveryTime = deliveryTime;
    }

    public void setItemId(long itemId) {
	this.itemId = itemId;
    }

    public void setItemName(String itemName) {
	this.itemName = itemName;
    }

    public void setNullString(String nullString) {
	this.nullString = nullString;
    }

    public void setWeight(BigDecimal weight) {
	this.weight = weight;
    }

}
