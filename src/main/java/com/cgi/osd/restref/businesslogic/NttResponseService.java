package com.cgi.osd.restref.businesslogic;

import java.math.BigDecimal;

import javax.enterprise.context.Dependent;

import org.joda.time.DateTime;
import org.joda.time.Period;

@Dependent
public class NttResponseService {

    public NttResponse getNttResponse() {
	final NttResponse response = new NttResponse();
	response.getItems().add(new NttResponseItem(1, "The first item", new DateTime(), new BigDecimal(1.101)));
	final DateTime secondDateTime = new DateTime().plus(Period.hours(1));
	response.getItems().add(new NttResponseItem(2, "The second item", secondDateTime, new BigDecimal(0.01923)));
	return response;
    }

}
