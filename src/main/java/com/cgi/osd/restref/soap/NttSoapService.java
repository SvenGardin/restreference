package com.cgi.osd.restref.soap;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;

import com.cgi.osd.restref.businesslogic.NttResponse;
import com.cgi.osd.restref.businesslogic.NttResponseService;

@Stateless
@WebService(wsdlLocation = "response.wsdl")
@HandlerChain(file = "/soap-handlers.xml")
public class NttSoapService {

    @Inject
    NttResponseService nttResponseService;

    @WebMethod
    public NttResponse getResponse() {
	final NttResponse response = this.nttResponseService.getNttResponse();
	return response;
    }

}
