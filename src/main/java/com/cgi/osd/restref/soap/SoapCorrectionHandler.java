package com.cgi.osd.restref.soap;

import java.util.Set;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class SoapCorrectionHandler implements SOAPHandler<SOAPMessageContext> {

    @Inject
    private XsltTransformer transformer;

    @Inject
    Logger logger;

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
	if ((boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY)) {
	    transformMessage(context);
	}
	return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
	return true;
    }

    @Override
    public void close(MessageContext context) {
    }

    @Override
    public Set<QName> getHeaders() {
	return null;
    }

    private void transformMessage(SOAPMessageContext context) {
	try {
	    final SOAPMessage soapMessage = context.getMessage();
	    final SOAPPart soapPart = soapMessage.getSOAPPart();
	    final Source inputSource = soapPart.getContent();

	    final Source result = this.transformer.transform(inputSource);
	    if (result != null) {
		soapPart.setContent(result);
		soapMessage.saveChanges();
	    }
	} catch (final SOAPException e) {
	    this.logger.severe("Soap error in handler: " + e.getMessage());
	}
    }

}
