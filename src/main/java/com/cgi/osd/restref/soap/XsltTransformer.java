package com.cgi.osd.restref.soap;

import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

@ApplicationScoped
public class XsltTransformer {

    @Inject
    private Logger logger;

    public Source transform(Source inputSource) {
	final Source firstResult = performOneTransformation(inputSource, "correct-sorting.xsl");
	final Source secondResult = performOneTransformation(firstResult, "correct-names-timezone.xsl");
	return secondResult;
    }

    private Source performOneTransformation(Source inputSource, String xsltName) {
	Source resultSource = null;
	try {
	    final InputStream xsltInputStream = this.getClass().getClassLoader().getResourceAsStream(xsltName);
	    if (xsltInputStream == null) {
		throw new TransformerConfigurationException("XSLT file " + xsltName + " not found.");
	    }
	    final Source xsltSource = new StreamSource(xsltInputStream);

	    final StreamResult result = new StreamResult(new StringWriter());

	    final TransformerFactory transformerFactory = TransformerFactory.newInstance();
	    final Transformer transformer = transformerFactory.newTransformer(xsltSource);
	    transformer.transform(inputSource, result);

	    final String resultString = result.getWriter().toString();
	    resultSource = new StreamSource(new StringReader(resultString));
	} catch (final TransformerConfigurationException e) {
	    this.logger.severe("XSLT transformer configuration failed: " + e.getMessage());
	} catch (final TransformerException e) {
	    this.logger.severe("XSLT transformation failed: " + e.getMessage());
	}
	return resultSource;

    }

}
