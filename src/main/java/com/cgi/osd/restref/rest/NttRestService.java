package com.cgi.osd.restref.rest;

import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.Form;

import com.cgi.osd.restref.businesslogic.NttResponse;
import com.cgi.osd.restref.businesslogic.NttResponseService;
import com.cgi.osd.restref.rest.resteasy.PostNordXml;
import com.cgi.osd.restref.rest.resteasy.QueryParameterMissingException;

/**
 * This class is a test bench for NTT RestEasy development.
 *
 */
@Path("ntt")
@RequestScoped
public class NttRestService {

    @Inject
    NttResponseService nttResponseService;

    @Inject
    Logger logger;

    @GET
    @Path("{var:shipment((Org)|)}")
    @PostNordXml
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public NttResponse findShipment(@QueryParam("fail") String fail) {
	if ("true".equals(fail)) {
	    throw new QueryParameterMissingException();
	}
	final NttResponse response = this.nttResponseService.getNttResponse();
	return response;
    }

    @GET
    @Path("multipleparam")
    @PostNordXml
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public NttResponse parseMultipleParams(@Form RequestParamsVo requestParams) {
	this.logger.info("mulipleparam called");
	final NttResponse response = this.nttResponseService.getNttResponse();
	return response;
    }
}
