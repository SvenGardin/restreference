package com.cgi.osd.restref.rest.resteasy;

import java.lang.annotation.Annotation;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;

import org.jboss.resteasy.annotations.DecorateTypes;
import org.jboss.resteasy.spi.interception.DecoratorProcessor;

/**
 * Within the RestEasy framework, this class is responsible for customizing the
 * JAXB marshaller according to Postnord's conventions.
 *
 */

@DecorateTypes(MediaType.APPLICATION_XML)
public class PostNordXmlProcessor implements DecoratorProcessor<Marshaller, PostNordXml> {

    @Override
    public Marshaller decorate(Marshaller target, PostNordXml annotation, @SuppressWarnings("rawtypes") Class type,
	    Annotation[] annotations, MediaType mediaType) {
	try {
	    target.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	    target.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	    target.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
	    target.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

	    // final URL schemaUrl =
	    // this.getClass().getResource("/response-schema.xsd");
	    // final SchemaFactory schemaFactory =
	    // SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	    // final Schema schema = schemaFactory.newSchema(schemaUrl);
	    // target.setSchema(schema);

	} catch (final PropertyException e) {
	    // This exception will never occur (most probably)
	}
	return target;
    }

}
