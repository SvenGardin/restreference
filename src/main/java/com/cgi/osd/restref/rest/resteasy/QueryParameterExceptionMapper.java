package com.cgi.osd.restref.rest.resteasy;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class QueryParameterExceptionMapper implements ExceptionMapper<QueryParameterMissingException> {

    @Override
    @Produces(MediaType.TEXT_HTML)
    public Response toResponse(QueryParameterMissingException exception) {
	final String errorPage = "<html>\n<head>\n\t<title>Status page</title>\n</head>\n<body style=\"font-family: sans-serif;\">\n"
		+ "<h3>Not Found</h3><p>You can get technical details <a href=\"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.5\">here</a>.<br/>\n"
		+ "Please continue your visit at our <a href=\"/\">home page</a>.\n</p>\n</body>\n</html>";
	final Response response = Response.status(Status.NOT_FOUND).entity(errorPage).type(MediaType.TEXT_HTML).build();
	return response;
    }
}
