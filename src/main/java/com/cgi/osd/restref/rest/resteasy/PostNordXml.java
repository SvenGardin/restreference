package com.cgi.osd.restref.rest.resteasy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.xml.bind.Marshaller;

import org.jboss.resteasy.annotations.Decorator;

/**
 * This annotation can be used for customizing REST method that produces
 * application/xml to Postnord's conventions.
 *
 */

@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Decorator(processor = PostNordXmlProcessor.class, target = Marshaller.class)
public @interface PostNordXml {

}
