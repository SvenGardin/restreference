package com.cgi.osd.restref.rest;

import java.util.Enumeration;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.ws.rs.core.MediaType;

/**
 * This class is responsible for providing a request modified to the requirements of the NTT REST API.
 *
 */
public class MediaTypeRequestWrapper extends HttpServletRequestWrapper {

    public MediaTypeRequestWrapper(HttpServletRequest request) {
	super(request);
    }

    @Override
    public Enumeration<String> getHeaderNames() {
	Enumeration<String> result;

	if (isMediaTypeInHeaders(super.getHeaderNames())) {
	    result = super.getHeaderNames();
	} else {
	    final Enumeration<String> headerNames = super.getHeaderNames();
	    final Vector<String> newHeaderNames = new Vector<>();
	    while (headerNames.hasMoreElements()) {
		newHeaderNames.add(headerNames.nextElement());
	    }
	    newHeaderNames.add("accept");
	    result = newHeaderNames.elements();
	}
	return result;
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
	Enumeration<String> result;
	if ("accept".equalsIgnoreCase(name)) {
	    result = getMediaType(name);
	} else {
	    result = super.getHeaders(name);
	}
	return result;
    }

    @Override
    public StringBuffer getRequestURL() {
	final StringBuffer urlBody = removeTypeSuffix(super.getRequestURL());
	return urlBody;
    }

    private Enumeration<String> getEnumFromString(String string) {
	final Vector<String> vector = new Vector<>();
	vector.add(string);
	final Enumeration<String> result = vector.elements();
	return result;
    }

    private Enumeration<String> getMediaType(String headerName) {
	Enumeration<String> result = super.getHeaders(headerName);
	final String requestUri = getRequestURI();
	final String mediaTypeFromUrl = getMediaTypeFromUri(requestUri);

	if (!mediaTypeFromUrl.isEmpty()) {
	    result = getMediaTypeEnumFromUri(requestUri);
	} else {
	    if (!isMediaTypeInHeaders(super.getHeaderNames())) {
		result = getEnumFromString(MediaType.APPLICATION_XML);
	    }
	}
	return result;
    }

    private Enumeration<String> getMediaTypeEnumFromUri(String requestURI) {
	return getEnumFromString(getMediaTypeFromUri(requestURI));
    }

    private String getMediaTypeFromUri(String requestURI) {
	if (requestURI.matches(".+\\.json$")) {
	    return MediaType.APPLICATION_JSON;
	}
	if (requestURI.matches(".+\\.xml$")) {
	    return MediaType.APPLICATION_XML;
	}
	return "";
    }

    private boolean isMediaTypeInHeaders(Enumeration<String> headers) {
	while (headers.hasMoreElements()) {
	    if ("accept".equalsIgnoreCase(headers.nextElement())) {
		return true;
	    }
	}
	return false;
    }

    private StringBuffer removeTypeSuffix(StringBuffer url) {
	final int urlBodyEnd = url.lastIndexOf(".");
	final int urlLength = url.length();
	final int suffixLength = url.length() - urlBodyEnd - 1;
	if (suffixLength > 4) {
	    return url;
	}
	final StringBuffer urlBody = url.delete(urlBodyEnd, urlLength);
	return urlBody;
    }

}
