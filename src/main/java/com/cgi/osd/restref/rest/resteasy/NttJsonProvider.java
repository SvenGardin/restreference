package com.cgi.osd.restref.rest.resteasy;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.Version;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.Module;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.introspect.VisibilityChecker;
import org.codehaus.jackson.map.module.SimpleModule;

/**
 * This class is responsible for providing required extensions to Jackson for serialization of NTT DTOs.
 *
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class NttJsonProvider extends JacksonJsonProvider {

    private Module getCustomSerializerModule() {
	final SimpleModule module = new SimpleModule("Custom serializer module", Version.unknownVersion());
	module.addSerializer(BigDecimal.class, new BigDecimalJsonSerializer());
	return module;
    }

    @PostConstruct
    private void init() {
	final ObjectMapper objectMapper = new ObjectMapper();
	objectMapper.setSerializationInclusion(Inclusion.NON_NULL);
	objectMapper.setSerializationInclusion(Inclusion.NON_EMPTY);
	objectMapper.configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true);
	objectMapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);
	objectMapper.registerModule(getCustomSerializerModule());

	// Exclude transient fields by basing serialization on fields and not on getters
	final VisibilityChecker<?> visibilityChecker = objectMapper.getSerializationConfig()
		.getDefaultVisibilityChecker().withFieldVisibility(Visibility.ANY)
		.withGetterVisibility(Visibility.NONE);
	objectMapper.setVisibilityChecker(visibilityChecker);

	setMapper(objectMapper);
    }

}
