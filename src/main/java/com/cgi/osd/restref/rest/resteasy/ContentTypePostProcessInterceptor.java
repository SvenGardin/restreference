package com.cgi.osd.restref.rest.resteasy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.interception.PostProcessInterceptor;

/**
 * This class is responsible for adding charset to the Content-Type header.
 *
 */

@Provider
@ServerInterceptor
public class ContentTypePostProcessInterceptor implements PostProcessInterceptor {

    @Override
    public void postProcess(ServerResponse response) {
	final MultivaluedMap<String, Object> headers = response.getMetadata();
	final List<Object> contentTypeList = headers.get("Content-Type");
	final MediaType contentType = (MediaType) contentTypeList.get(0);
	final String type = contentType.getType();
	final String subType = contentType.getSubtype();
	final Map<String, String> charsetMap = new HashMap<>();
	charsetMap.put(MediaType.CHARSET_PARAMETER, "UTF-8");
	final MediaType modifiedContentType = new MediaType(type, subType, charsetMap);
	contentTypeList.clear();
	contentTypeList.add(modifiedContentType);
	headers.put("Content-Type", contentTypeList);
    }

}
