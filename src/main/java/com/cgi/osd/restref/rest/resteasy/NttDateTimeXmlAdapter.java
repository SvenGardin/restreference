package com.cgi.osd.restref.rest.resteasy;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * This class is responsible for providing the correct marshal of Joda date time for the NTT REST API.
 *
 */
public class NttDateTimeXmlAdapter extends XmlAdapter<String, DateTime> {

    private static DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mmZ");

    @Override
    public String marshal(DateTime dateTime) throws Exception {
	final String dateTimeString = NttDateTimeXmlAdapter.formatter.print(dateTime);
	return dateTimeString;
    }

    @Override
    public DateTime unmarshal(String v) throws Exception {
	return new DateTime(v);
    }

}
