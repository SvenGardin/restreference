package com.cgi.osd.restref.rest;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.QueryParam;

/**
 * This class is an value object class for the request query parameters.
 *
 */
public class RequestParamsVo {

    @QueryParam("channelId")
    private String channelId;

    @QueryParam("locale")
    private String locale;

    @QueryParam("shippingId")
    private String shippingId;

    @HeaderParam("Accept")
    private String accept;

    public String getChannelId() {
	return this.channelId;
    }

    public void setChannelId(String channelId) {
	this.channelId = channelId;
    }

    public String getLocale() {
	return this.locale;
    }

    public void setLocale(String locale) {
	this.locale = locale;
    }

    public String getShippingId() {
	return this.shippingId;
    }

    public void setShippingId(String shippingId) {
	this.shippingId = shippingId;
    }

    public String getAccept() {
	return this.accept;
    }

    public void setAccept(String accept) {
	this.accept = accept;
    }

}
