package com.cgi.osd.restref.rest;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

@WebFilter(filterName = "MediaTypeFilter", urlPatterns = { "/rest/ntt/*" })
public class MediaTypeFilter implements Filter {

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	final HttpServletRequestWrapper modifiedRequest = new MediaTypeRequestWrapper((HttpServletRequest) request);
	chain.doFilter(modifiedRequest, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

}
